/**
 * Created by Fara Aileen on 7/4/2016.
 */
(function() {
    "use strict";
    var CartApp = angular.module("CartApp", []);
    var CartController = function($log) {
        var cart = this;
        var found = false;
        var i;

        cart.item = "";
        cart.quantity = 1;
        cart.items = [];
        cart.addToCart = function() {
            while((i < cart.items.length) && !found){
                if(cart.items[i].item == cart.item){
                    console.log("in here");
                    found = true;
                    cart.items[i].quantity += cart.quantity;
                    break;
                }
                i++;
            }
            if (!found) {
                cart.items.push({
                    item: cart.item,
                    quantity: cart.quantity
                });
            }

            $log.info("Added " + cart.quantity + " item of " + cart.item + " to cart");
            cart.item = "";
            cart.quantity = 1;
            i = 0;
            found = false;
        };
        cart.removeFromCart = function($index) {
            cart.items.splice($index, 1);
        }
    };
    CartApp.controller("CartController", ["$log", CartController]);
})();